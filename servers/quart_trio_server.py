from quart import request
from quart_trio import QuartTrio

from servers.common import fib

app = QuartTrio(__name__)


@app.route('/<int:number>')
async def index(number=1):
    return "{}-fib({})={}".format(__file__, number, fib(int(number)))


@app.route('/', methods=['POST'])
async def post():
    data = await request.form
    number = data['fib']
    return "{}-fib({})={}".format(__file__, number, fib(int(number)))


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000)
