import sys

from aiohttp import web

from servers.common import fib

app = web.Application()


async def index(request):
    number = request.match_info['number']
    return web.Response(text="{}-fib({})={}".format(__file__, number, fib(int(number))))


async def post(request):
    data = await request.json()
    number = data['fib']
    return web.Response(text="{}-fib({})={}".format(__file__, number, fib(int(number))))


async def post2(request):
    data = await request.json()
    text = f"{__file__}-sizeof payload={sys.getsizeof(data)}--content-length={request.content_length}"

    return web.Response(text=text)

app.router.add_get('/{number:\d+}', index)
app.router.add_post('/', post)
app.router.add_post('/post2', post2)

if __name__ == '__main__':
    web.run_app(app, host='127.0.0.1', port=5000)
