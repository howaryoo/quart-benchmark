import sys

from flask import Flask, request

from servers.common import fib

app = Flask(__name__)


@app.route('/<int:number>')
def index(number=1):
    return "{}-fib({})={}".format(__file__, number, fib(int(number)))


@app.route('/', methods=['POST'])
def post():
    number = request.json['fib']
    return "{}-fib({})={}".format(__file__, number, fib(int(number)))


@app.route("/post2", methods=["POST"])
def post2():
    data = request.json
    text = f"{__file__}-sizeof payload={sys.getsizeof(data)}--content-length={request.content_length}"
    return text


if __name__ == '__main__':
    app.run(debug=False, host='127.0.0.1', port=5000)
