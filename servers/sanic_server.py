from sanic import response, Sanic

from servers.common import fib

app = Sanic()


@app.route('/<number:int>')
async def index(request, number=1):
    return response.html("{}-fib({})={}".format(__file__, number, fib(int(number))))


@app.route('/', methods=['POST'])
async def post(request):
    data = request.form
    number = data['fib'][0]
    return response.html("{}-fib({})={}".format(__file__, number, fib(int(number))))


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000)
