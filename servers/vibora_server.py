import json

from vibora import Vibora, Request
from vibora.responses import Response

from servers.common import fib

app = Vibora()


@app.route('/<n>', methods=['GET'])
async def get(n: int):
    return Response(f"{__file__}-fib({n})={fib(int(n))}".encode("utf8"))


@app.route('/', methods=['POST'])
async def post(request: Request):
    content = await request.stream.read()
    n = json.loads(content.decode('utf8'))['fib']
    return Response(f"{__file__}-fib({n})={fib(int(n))}".encode("utf8"))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
