from gevent import monkey, pywsgi

from servers.common import fib

monkey.patch_all()
import falcon

app = falcon.API()


class Get:
    def on_get(self, req, resp, *, n):
        resp.body = f"{__file__}-fib({n})={fib(int(n))}"
        resp.content_type = falcon.MEDIA_TEXT


class Post:
    def on_post(self, req, resp):
        n = req.media['fib']
        resp.body = f"{__file__}-fib({n})={fib(int(n))}"
        resp.content_type = falcon.MEDIA_TEXT


get = Get()
post = Post()

app.add_route("/{n:int}", get)
app.add_route("/", post)
