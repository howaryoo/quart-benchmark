import sys

import falcon

from servers.common import fib

app = falcon.API()
app.req_options.auto_parse_form_urlencoded = True


class Get:
    def on_get(self, req, resp, *, n):
        resp.body = f"{__file__}-fib({n})={fib(int(n))}"
        resp.content_type = falcon.MEDIA_TEXT


class Post:
    def on_post(self, req, resp):
        n = req.media['fib']

        resp.body = f"{__file__}-fib({n})={fib(int(n))}"
        resp.content_type = falcon.MEDIA_TEXT


class Post2:
    def on_post(self, req, resp):
        resp.body = f"{__file__}-sizeof payload={sys.getsizeof(req.media)}--content-length={req.content_length}"
        resp.content_type = falcon.MEDIA_TEXT


get = Get()
post = Post()
post2 = Post2()

app.add_route("/{n:int}", get)
app.add_route("/", post)
app.add_route("/post2", post2)


if __name__ == '__main__':
    from wsgiref import simple_server
    import os

    with simple_server.make_server('', os.getenv('PORT', 5000), app) as httpd:
        httpd.serve_forever()
