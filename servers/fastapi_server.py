import sys

import uvicorn
from fastapi import FastAPI
from starlette.responses import PlainTextResponse

from servers.common import fib

app = FastAPI()


@app.route("/{n}")
async def on_get(req):
    n = req.path_params['n']
    text = f"{__file__}-fib({n})={fib(int(n))}"
    return PlainTextResponse(text)


@app.route("/", methods=["POST"])
async def on_post(req):
    data = await req.json()
    n = data['fib']
    text = f"{__file__}-fib({n})={fib(int(n))}"
    return PlainTextResponse(text)


@app.route("/post2", methods=["POST"])
async def on_post2(req):
    data = await req.json()
    text = f"{__file__}-sizeof payload={sys.getsizeof(data)}--content-length={req.content_length}"
    return PlainTextResponse(text)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=5000, loop='asyncio')
