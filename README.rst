Benchmark
=========

This allows a simple benchmark comparison of a few Python web
frameworks. It is mainly useful to compare Quart against Flask.

Usage
-----

This expects that `wrk <https://github.com/wg/wrk>`_ is installed and
available on the path. If so,::

    pipenv install
    python benchmark.py


Test with curl
--------------

    $ curl http://localhost:5000/10
    ...fastapi_server.py-fib(10)=55

    $ curl -H "Content-Type: application/json" -X POST -d '{"fib": 100}' http://localhost:5000/post

    or provide payload as a file:

    $    curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d @payload2.json http://localhost:5000/post2


Example output
--------------

output::

    get fib(10) requests/second
    ###################################################################################
    ██                                                    745  falcon
    ███                                                  1004  flask
    ████                                                 1393  flask-cheroot
    ███████                                              2463  flask-gunicorn-sync
    ████████                                             2673  flask-gunicorn-eventlet
    ██████████████                                       4704  fastapi
    ████████████████                                     5331  aiohttp
    ██████████████████                                   6045  flask-gunicorn-meinheld
    ████████████████████                                 6663  fastapi-uvicorn
    ████████████████████████████                         9487  aiohttp-gunicorn-uvloop
    ██████████████████████████████████                  11353  fastapi-gunicorn-uvicorn
    ██████████████████████████████████████████████████  16408  falcon-gunicorn-meinheld
    post fib(10) requests/second
    ###################################################################################
    ██                                                    659  flask
    ███                                                   887  falcon
    █████                                                1390  flask-cheroot
    ██████                                               1730  flask-gunicorn-sync
    ████████                                             2111  flask-gunicorn-eventlet
    ███████████████                                      4101  fastapi
    █████████████████                                    4629  flask-gunicorn-meinheld
    ███████████████████                                  5143  aiohttp
    ████████████████████                                 5370  fastapi-uvicorn
    ███████████████████████████████                      8228  aiohttp-gunicorn-uvloop
    █████████████████████████████████                    8618  fastapi-gunicorn-uvicorn
    ██████████████████████████████████████████████████  12946  falcon-gunicorn-meinheld
    post 64k JSON payload requests/second
    ##################################################################################
    █████████████████                                    586  flask
    ██████████████████                                   620  falcon
    ██████████████████                                   625  flask-cheroot
    ███████████████████████                              792  flask-gunicorn-sync
    ███████████████████████                              808  flask-gunicorn-eventlet
    ███████████████████████████                          935  fastapi
    ██████████████████████████████                      1027  fastapi-uvicorn
    ███████████████████████████████                     1084  fastapi-gunicorn-uvicorn
    ███████████████████████████████████████████         1488  aiohttp
    █████████████████████████████████████████████       1557  flask-gunicorn-meinheld
    █████████████████████████████████████████████████   1688  falcon-gunicorn-meinheld
    ██████████████████████████████████████████████████  1705  aiohttp-gunicorn-uvloop

with 4 threads
--------------

output::

    get fib(10) requests/second
    ###################################################################################
    ███                                                  1041  falcon
    ███                                                  1074  flask
    █████                                                1467  flask-cheroot
    ██████                                               1795  flask-gunicorn-sync
    ███████                                              2216  flask-gunicorn-eventlet
    ████████████████                                     4764  fastapi
    ██████████████████                                   5450  aiohttp
    ███████████████████                                  5821  fastapi-uvicorn
    ████████████████████                                 6025  flask-gunicorn-meinheld
    ██████████████████████████████                       8816  aiohttp-gunicorn-uvloop
    ████████████████████████████████████████            11752  fastapi-gunicorn-uvicorn
    ██████████████████████████████████████████████████  14567  falcon-gunicorn-meinheld
    post fib(10) requests/second
    ###################################################################################
                                                           86  flask
    █                                                     310  falcon
    █████                                                1080  flask-gunicorn-sync
    ██████                                               1288  flask-cheroot
    ████████                                             1777  flask-gunicorn-eventlet
    ████████████████████                                 4379  fastapi
    █████████████████████                                4467  flask-gunicorn-meinheld
    █████████████████████                                4635  aiohttp
    ███████████████████████                              4951  fastapi-uvicorn
    ███████████████████████████████████                  7500  aiohttp-gunicorn-uvloop
    ███████████████████████████████████████████          9295  fastapi-gunicorn-uvicorn
    █████████████████████████████████████████████████   10625  falcon-gunicorn-meinheld
    post 64k JSON payload requests/second
    ##################################################################################
                                                           0  flask
    ███████████████████                                  642  flask-gunicorn-sync
    █████████████████████                                713  flask-cheroot
    ██████████████████████                               743  flask-gunicorn-eventlet
    ███████████████████████                              776  falcon
    ███████████████████████████                          929  fastapi
    ████████████████████████████                         956  fastapi-uvicorn
    █████████████████████████████████                   1142  fastapi-gunicorn-uvicorn
    ██████████████████████████████████████              1312  aiohttp
    █████████████████████████████████████████           1415  flask-gunicorn-meinheld
    ███████████████████████████████████████████         1472  aiohttp-gunicorn-uvloop
    ██████████████████████████████████████████████████  1685  falcon-gunicorn-meinheld
