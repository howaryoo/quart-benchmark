import responder

from servers.common import fib

app = responder.API()


@app.route("/{n}")
class Get:
    async def on_get(self, req, resp, *, n):
        resp.text = f"{__file__}-fib({n})={fib(int(n))}"


@app.route("/")
class Post:
    async def on_post(self, req, resp):
        data = await req.media()
        n = data['fib']
        resp.text = f"{__file__}-fib({n})={fib(int(n))}"


if __name__ == "__main__":
    app.run(address="127.0.0.1", port=5000)
