import uvicorn
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse

from servers.common import fib

app = Starlette()


@app.route("/{n}")
async def on_get(req):
    n = req.path_params['n']
    text = f"{__file__}-fib({n})={fib(int(n))}"
    return PlainTextResponse(text)


@app.route("/", methods=["POST"])
async def on_post(req):
    data = await req.form()
    n = data['fib']
    text = f"{__file__}-fib({n})={fib(int(n))}"
    return PlainTextResponse(text)


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=5000, loop='asyncio')
