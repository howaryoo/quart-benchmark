import logging
import re
import subprocess
import sys
from collections import defaultdict, namedtuple
from datetime import datetime
from enum import auto, Enum
from time import sleep

import requests
from ascii_graph import Pyasciigraph

logger = logging.getLogger(__name__)


class ServerType(Enum):
    daphne = auto()
    direct = auto()
    gunicorn = auto()
    hypercorn = auto()
    uvicorn = auto()


Server = namedtuple('Server', ['module', 'server_type', 'settings'])

SERVERS = {
    'fastapi': Server('fastapi_server', ServerType.direct, []),
    'fastapi-uvicorn': Server('fastapi_server', ServerType.uvicorn, []),
    'fastapi-gunicorn-uvicorn': Server('fastapi_server',
                                        ServerType.gunicorn,
                                        ['--worker-class', 'uvicorn.workers.UvicornWorker']),
    # 'fastapi-gunicorn-uvicorn_w17': Server('fastapi_server',
    #                                         ServerType.gunicorn,
    #                                         ['-w', '17', '--worker-class', 'uvicorn.workers.UvicornWorker']),

    # 'responder': Server('responder_server', ServerType.direct, []),
    # 'responder-uvicorn': Server('responder_server', ServerType.uvicorn, []),
    # 'responder-gunicorn-uvicorn': Server('responder_server',
    #                                      ServerType.gunicorn,
    #                                      ['--worker-class', 'uvicorn.workers.UvicornWorker']),
    #
    # 'starlette': Server('starlette_server', ServerType.direct, []),
    # 'starlette-uvicorn': Server('starlette_server', ServerType.uvicorn, []),
    # 'starlette-gunicorn-uvicorn': Server('starlette_server',
    #                                      ServerType.gunicorn,
    #                                      ['--worker-class', 'uvicorn.workers.UvicornWorker']),
    'falcon': Server('falcon_server', ServerType.direct, []),
    # 'falcon-gunicorn-meinheld-w17': Server('falcon_server', ServerType.gunicorn,
    #                                       ['-w', '17', '--worker-class', 'meinheld.gmeinheld.MeinheldWorker']),
    'falcon-gunicorn-meinheld': Server('falcon_server',
                                       ServerType.gunicorn,
                                       ['--worker-class', 'meinheld.gmeinheld.MeinheldWorker']),
    'aiohttp': Server('aiohttp_server', ServerType.direct, []),
    'aiohttp-gunicorn-uvloop': Server('aiohttp_server',
                                      ServerType.gunicorn,
                                      ['--worker-class', 'aiohttp.worker.GunicornUVLoopWebWorker']),
    'flask': Server('flask_server', ServerType.direct, []),
    'flask-cheroot': Server('flask_cheroot_server', ServerType.direct, []),
    'flask-gunicorn-sync': Server('flask_server', ServerType.gunicorn, ['--worker-class', 'sync']),
    'flask-gunicorn-eventlet': Server('flask_server', ServerType.gunicorn, ['--worker-class', 'eventlet']),
    'flask-gunicorn-meinheld': Server('flask_server',
                                      ServerType.gunicorn,
                                      ['--worker-class', 'meinheld.gmeinheld.MeinheldWorker']),
    # 'quart': Server('quart_server', ServerType.direct, []),
    # 'quart-daphne': Server('quart_server', ServerType.daphne, []),
    # 'quart-hypercorn': Server('quart_server', ServerType.hypercorn, ['--worker-class', 'uvloop']),
    # # 'quart-trio': Server('quart_trio_server', ServerType.hypercorn, ['--worker-class', 'trio']),
    # # 'quart-uvicorn': Server('quart_server', ServerType.uvicorn, []),
    # # 'sanic': Server('sanic_server', ServerType.direct, []),
    # # 'sanic-gunicorn-uvloop': Server('sanic_server', ServerType.gunicorn, ['--worker-class', 'sanic.worker.GunicornWorker']),
    # # 'vibora': Server('vibora_server', ServerType.direct, []),
}

REQUESTS_SECOND_RE = re.compile(r'Requests\/sec\:\s*(?P<reqsec>\d+\.\d+)(?P<unit>[kMG])?')
UNITS = {
    'k': 1_000,
    'M': 1_000_000,
    'G': 1_000_000_000,
}
HOST = '127.0.0.1'
PORT = 5000


def run_server(server):
    if server.server_type == ServerType.gunicorn:
        return subprocess.Popen(
            ['gunicorn', "{}:app".format(server.module), '-b', "{}:{}".format(HOST, PORT)] + server.settings,
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
            cwd='servers',
        )
    elif server.server_type == ServerType.uvicorn:
        return subprocess.Popen(
            ['uvicorn', "{}:app".format(server.module), '--host', HOST, '--port', str(PORT)] + server.settings,
            cwd='servers', stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
        )
    elif server.server_type == ServerType.daphne:
        return subprocess.Popen(
            ['daphne', "{}:app".format(server.module), '-b', HOST, '-p', str(PORT)] + server.settings,
            cwd='servers', stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
        )
    elif server.server_type == ServerType.hypercorn:
        return subprocess.Popen(
            ['hypercorn', "{}:app".format(server.module), '-b', "{}:{}".format(HOST, PORT)] + server.settings,
            cwd='servers', stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
        )
    elif server.server_type == ServerType.direct:
        return subprocess.Popen(
            ['python', "{}.py".format(server.module)] + server.settings,
            cwd='servers', stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL,
        )
    else:
        raise ValueError("Unknown server {}".format(server))


def test_server(server):
    response = requests.get("http://{}:{}/10".format(HOST, PORT))
    try:
        assert response.status_code == 200
        assert server.module in response.text
    except AssertionError:
        logger.error("server.module: %s, response.text: %s", server.module, response.text, exc_info=True)

    response = requests.post("http://{}:{}/".format(HOST, PORT), json={"fib": 10})
    try:
        assert response.status_code == 200
        assert server.module in response.text
    except AssertionError:
        logger.error("server.module: %s, response.text: %s", server.module, response.text, exc_info=True)


def run_benchmark(path, script=None):
    if script is not None:
        script_cmd = "-s {}".format(script)
    else:
        script_cmd = ''
    output = subprocess.check_output(
        "wrk -t 4 -c 64 -d 30s {} http://{}:{}/{}".format(script_cmd, HOST, PORT, path), shell=True,
    )
    match = REQUESTS_SECOND_RE.search(output.decode())
    requests_second = float(match.group('reqsec'))
    if match.group('unit'):
        requests_second = requests_second * UNITS[match.group('unit')]
    return requests_second


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout,
                        format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d:%H:%M:%S',
                        level=logging.INFO)

    results = defaultdict(list)
    for name, server in SERVERS.items():
        try:
            print("Testing {} {}".format(name, datetime.now().isoformat()))
            process = run_server(server)
            sleep(5)
            test_server(server)
            results['get fib(10)'].append((name, run_benchmark('10')))
            results['post fib(10)'].append((name, run_benchmark('', 'scripts/post.lua')))
            results['post 64k JSON payload'].append((name, run_benchmark('post2', 'scripts/post2.lua')))
        finally:
            process.terminate()
            process.wait()
    graph = Pyasciigraph()
    for key, value in results.items():
        for line in graph.graph("{} requests/second".format(key), sorted(value, key=lambda result: result[1])):
            print(line)
